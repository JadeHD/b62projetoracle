#include "gestionconsole.h"

void clearScreen()
{
	system("cls");
}

char getUpperChar()
{
	int keyCode{ _getch() };				// _getch must be call twice to catch the key code
	int functionKey{ _getch() };			// and the function key
	return (char)toupper(keyCode);
}
int askQuestionReturnInt(string question, int min, int max)
{
	int reponse{};
	printf("\n\n%s", question);
	std::cin >> reponse;
	while (!(reponse <= max && reponse >= min)){
		std::cin >> reponse ;
	}
	return reponse;

}
void showWelcomeScreen()
{
	printf("\n\n\n%s\n%s\n\n\n%s\n", mainTitle, subTitle, menu);
	getUpperChar();


}

bool isQuitting()
{
	clearScreen();
	printf("%s", questionToQuit);

	return getUpperChar() == 'O';
}

void showSpaceTime(bool *  spaceTime, bool showTime)
{
	if (spaceTime) {
		for (size_t time{}; time < timeSize; ++time) {
			printf("%02zd   ", time);
			for (size_t space{}; space < spaceSize; ++space) {
				printf("%c", spaceTime[time][space] ? stateActive : stateInactive);
			}
			printf("\n");
		}
	}
}

void showTic(SpaceTime spaceTime, size_t ruleValue)
{
	char ruleBinValue[256];
	int2bin((int)ruleValue, 8, ruleBinValue, 256);

	clearScreen();
	printf("%s\n", lineTitle);
	printf("Current rule : %zd (%s)\n", ruleValue, ruleBinValue);
	printf("%s\n", lineTitle);
	showSpaceTime(spaceTime, true);
}

bool getTicInputUser(size_t * rule)
{
	if (!rule) {
		return true;
	}

	bool quitting{ false };
	switch (getUpperChar()) {
	case 'Q':
		quitting = isQuitting();
	case 'R':
		break;
	default:
		*rule = (*rule + 1) % ruleCount;
	}

	return !quitting;
}