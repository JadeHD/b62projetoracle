#ifndef TRAITEMENT_H

#define TRAITEMENT_H
#include "main.h"
#include "utilitaire.h"
#include "gestionconsole.h"
void initializeSpaceTime(bool * spaceTime);			// Initialise toute la simulation � false
void randomizeState(bool * cell);						// D�termine al�atoirement l'�tat d'une cellule
void randomizeSpace(bool *  space);						// Initialise l'espace al�atoirement
void setRule(bool *  rule, size_t ruleValue);
bool tic(bool *  spaceTime, size_t * rule);			// Effectue un pas de simulation
int getNeighborhoodValue(bool *  spaceTime, size_t currentTime, size_t currentSpace); // Retourne la valeur du voisinage de la cellule � traiter (8 valeurs possibles entre 000 et 111)
void evolveOverSpace(bool *  spaceTime, bool *  rule, size_t currentTime);	// Effectue l'�volution pour un temps donn�
void evolveOverTime(bool *  spaceTime, bool *  rule);		// Effectue l'�volution pour toute la simulation
void ticSpaceTime(bool *  spaceTime, size_t ruleValue);	// Effectue l'assignation de la r�gle et effectue l'�volution


#endif // !TRAITEMENT_H


