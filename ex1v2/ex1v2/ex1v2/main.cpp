#include "main.h"

int main()
{
	// Initialisation du g�n�rateur de nombre al�atoire
	srand(unsigned int(time(nullptr)));

	// Conteneur de la simulation et initialisation
	//const size_t spaceSize{ 80 };					    		// Taille de l'espace 
	//const size_t evolutionCount{24  };						// Nombre d'�volutions
	spaceSize = askQuestionReturnInt(questionTailleUnivers, 20, 80);
	evolutionCount = askQuestionReturnInt(questionTempsSimulation, 5, 250);
	timeSize = evolutionCount + 1;


	size_t currentRule{};
	bool * spaceTime{new bool[timeSize]};
	initializeSpaceTime(spaceTime);

	// Message d'accueil
	showWelcomeScreen();
	
	// Boucle de traitement
	while (tic(spaceTime, &currentRule));

	return 0;
}

/*
int main()
{
	initMenu();

	affichageResultat();

	return 0;
}
*/