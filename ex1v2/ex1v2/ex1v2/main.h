#ifndef MAIN_H

#define MAIN_H
#include <cmath>
#include <time.h>
#include <cctype>

size_t spaceSize{  };					    		// Taille de l'espace 
size_t evolutionCount{24  };						// Nombre d'�volutions
size_t timeSize{ evolutionCount+1 };			// Taille du temps
const size_t ruleSize{ 8 };								// Taille d'une r�gle
const size_t ruleCount{ (size_t)pow(2, ruleSize) };		// Nombre de r�gles possibles
const char stateActive{ '*' };							// Caract�re repr�sentant un �tat actif
const char stateInactive{ ' ' };
// D�finition des types
/*
typedef bool Space[spaceSize];							// Type repr�sentant l'espace
typedef Space SpaceTime[timeSize];						// Type repr�sentant l'espace dans le temps
typedef bool Rule[ruleSize];							// Type repr�sentant une r�gle
*/

#include "traitementautomate.h"

#endif MAIN_H
