#include "traitementautomate.h"

void initializeSpaceTime(bool *  spaceTime)
{
	if (spaceTime) {
		memset(spaceTime, 0, sizeof(bool) * spaceSize * timeSize);
	}
}

void randomizeSpace(bool *  space)
{
	if (space) {
		for (size_t iSpace{}; iSpace < spaceSize; ++iSpace) {
			randomizeState(space + iSpace);
		}
	}
}

void setRule(bool *  rule, size_t ruleValue)
{
	if (rule && ruleValue < ruleCount) {
		for (size_t r{}; r < ruleSize; ++r) {
			rule[r] = (ruleValue >> r) & 1;
		}
	}
}

void randomizeState(bool * cell)
{
	if (cell) {
		*cell = randomEvent();
	}
}

bool tic(bool *  spaceTime, size_t * rule)
{
	if (!rule) {
		return false;
	}

	ticSpaceTime(spaceTime, *rule);
	showTic(spaceTime, *rule);
	return getTicInputUser(rule);
}

// Cette fonction ne fait aucune validation des paramètres d'entrée.
// C'est la responsabilité de la fonction appelante de faire cette validation.
// Préconditions :	spaceTime must be valid
//					currentTime	 E [0, tSize[
//					currentSpace E [1, tSize - 1[
int getNeighborhoodValue(bool * spaceTime, size_t currentTime, size_t currentSpace)
{
	return	(int(spaceTime[currentTime][currentSpace + 1]) << 0) |
		(int(spaceTime[currentTime][currentSpace]) << 1) |
		(int(spaceTime[currentTime][currentSpace - 1]) << 2);
}

void evolveOverSpace(bool *  spaceTime, bool *  rule, size_t currentTime)
{
	if (spaceTime && currentTime > 0 && currentTime < timeSize && rule) {
		spaceTime[currentTime][0] = spaceTime[currentTime][spaceSize - 1] = false;
		for (size_t space{ 1 }; space < spaceSize - 1; ++space) {
			spaceTime[currentTime][space] = rule[getNeighborhoodValue(spaceTime, currentTime - 1, space)];
		}
	}
}

void evolveOverTime(bool *  spaceTime, bool *  rule)
{
	if (spaceTime && rule) {
		randomizeSpace(spaceTime[0]);
		for (size_t time{ 1 }; time < timeSize; ++time) {
			evolveOverSpace(spaceTime, rule, time);
		}
	}
}

void ticSpaceTime(bool *  spaceTime, size_t ruleValue)
{
	if (spaceTime && ruleValue < ruleCount) {
		bool * rule{new bool [ruleSize]};
		setRule(rule, ruleValue);
		evolveOverTime(spaceTime, rule);
	}
}