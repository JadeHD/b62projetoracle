#ifndef GESTIONCONSOLE_H

#define GESTIONCONSOLE_H
#include <iostream>
#include <conio.h>
#include "main.h"
#include <cstring>

using namespace std;
													// D�finition des cha�nes de caract�res utilis�es
const char mainTitle[]{ "ACE" };
const char subTitle[]{ "Logiciel de simulation d'un automate cellulaire elementaire" };
const char lineTitle[]{ "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" };
const char menu[]{ "Menu :\n\t(Q)uitter\n\t(R)eprendre la regle\n\t(...) toute autre touche pour poursuivre\n\n\nAppuyez sur une touche pour poursuivre..." };
const char questionToQuit[]{ "Voulez-vous quitter?\n\n\t(O)ui\n\t(...) toute autre touche pour poursuivre" };
const char questionTailleUnivers[]{ "Choisir la taille de l'univers (entre 20 et 80) : " };
const char questionTempsSimulation[]{ "Choisir le temps de la simulation : entre 5 et 250 : " };
void clearScreen();										// Efface la console
char getUpperChar();									// Fonction retournant la majuscule d'un caract�re lu � la console
bool isQuitting();										// Retourne si l'usager confirme son d�sire de quitter l'application
void showWelcomeScreen();								// Affiche la page d'accueil
//void showSpaceTime(SpaceTime spaceTime, bool showTime);	// Affiche l'image cr��e par une simulation
//void showTic(SpaceTime spaceTime, size_t ruleValue);	// Affiche la r�gle courante et l'image cr��e par la simulation
bool getTicInputUser(size_t * rule);					// Demande � l'usager quelle instruction faire 
int askQuestionReturnInt(string question, int min, int max); //demande question et retourn la valeur entrer et verifie que le nombre est dans l'interval sp�cifi�
#endif GESTIONCONSOLE_H

