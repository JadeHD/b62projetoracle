#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <bitset>

using namespace std;

 int CELLULE {};
 int NB_EVOLUTION {};
const int NBREGLE {255};
const char alive = '*';
const char dead = ' ';
const char questionTailleUnivers[]{ "Choisir la taille de l'univers (entre 20 et 80) : " };
const char questionTempsSimulation[]{ "Choisir le temps de la simulation : entre 5 et 250 : " };

int askQuestionReturnInt(string question, int min, int max)
{
	int reponse{};
	cout << "\n"  << question;
	cin >> reponse;
	while (!(reponse <= max && reponse >= min)) {
		cin >> reponse;
	}
	return reponse;
}

void initMenu()
{
	cout << "Automate cellulaire elementaire" << endl << endl;
	cout << "q ou Q : quitte le programme apres confirmation" << endl;
	cout << "r ou R : la regle reste la m�me" << endl;
	cout << "toute autre touche : incr�mente la regle de 1 (circulaire)" << endl;
	CELLULE = askQuestionReturnInt(questionTailleUnivers, 20, 80);
	NB_EVOLUTION = askQuestionReturnInt(questionTempsSimulation, 5, 250);
}


bool resultatAleatoire()
{
	return rand() % 2;
}

void showLine(bool * line)
{
	char value{};
	for (int i{ 0 }; i < CELLULE; ++i) 
	{
		value = (*(line + i)) ? alive : dead;
		cout << value;
	}
	cout << endl;
}


void iniTab(bool * line)
{
	*(line) = 0;
	*(line+CELLULE - 1) = 0;
	for (int i{ 1 }; i < CELLULE - 1; ++i) {
		*(line + i) = resultatAleatoire();
	}
}

char getAndClear()
{
	char entree = _getch();
	system("CLS");
	return entree;
}


void traitementBit(bool * line, int compteur)
{
	bitset<3> compare;
	bitset<8> regle = compteur;
	int temp {0};
	for (int y{ 1 }; y <CELLULE - 1; ++y)
	{
		compare.set(2, temp);
		compare.set(1, *(line + y));
		compare.set(0, *(line + y + 1));
		temp = *(line + y);
		*(line + y) = regle[compare.to_ulong()];
	}
}


bitset<8> regleToBin(int noRegle)
{
	bitset<8> regle;
	regle = noRegle;
	return regle;
}


void affichageResultat()
{
	char entree{};
	bool * line{new bool[CELLULE]};
	int compteur = 0;
	entree = getAndClear();

	while (entree != 'q')   
	{
		cout << "voici la r�gle num�ro " << compteur << " en binaire " << regleToBin(compteur) << endl << endl;
		
		//iniTab
		iniTab(line);
		
		showLine(line);
		for (int i{ 1 }; i <NB_EVOLUTION; ++i) 
		{
			traitementBit(line, compteur);
			cout << "Ligne " << i << " ";
			showLine(line);
		}
		entree = getAndClear();

		if (entree != 'r')
		{
			compteur++;
		}
		else if (compteur == NBREGLE)
		{
			compteur = 0;
		}
	}
}

int main()
{
	initMenu();

	affichageResultat();

	return 0;
}