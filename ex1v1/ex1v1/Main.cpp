#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <bitset>

using namespace std;

const int CELLULE {80};
const int NB_EVOLUTION {24};
const int NBREGLE {255};




void initMenu()
{
	cout << "Automate cellulaire elementaire" << endl << endl;
	cout << "q ou Q : quitte le programme apres confirmation" << endl;
	cout << "r ou R : la regle reste la m�me" << endl;
	cout << "toute autre touche : incr�mente la regle de 1 (circulaire)" << endl;
	cout << "toucher � une touche pour continuer";

}

bool resultatAleatoire()
{
	return rand() % 2;
}

void showLine(bool line[])
{
	char value{};
	for (int i{ 0 }; i < CELLULE; ++i) 
	{
		value = (line[i]) ? '*' : ' ';
		cout << value;
	}
	cout << endl;
}

void affichageResultat()
{
	char entree{};
	bool line[CELLULE];
	int compteur = 0;
	bitset<3> compare;
	bitset<8> regle;
	int temp{};
	entree = _getch();
	system("CLS");

	while (entree != 'q')   
	{
		regle = compteur;
		cout << "voici la r�gle num�ro " << compteur << " en binaire " << regle << endl << endl;
		
		//iniTab
		line[0] = 0;
		line[CELLULE - 1] = 0;
		for (int i{ 1 }; i < CELLULE-1; ++i) {
			line[i] = resultatAleatoire();
		}
		
		showLine(line);
		for (int i{ 1 }; i <NB_EVOLUTION; ++i) 
		{
			temp = 0;
			for (int y{ 1 }; y <CELLULE-1; ++y) 
			{
				compare.set(2, temp);
				compare.set(1, line[y ]);
				compare.set(0, line[y + 1]);
				temp = line[y ];
				line[y] = regle[compare.to_ulong()];
			}
			cout << "Ligne " << i << " ";
			showLine(line);
		}
		entree = _getch();
		system("CLS");
		if (entree != 'r')
		{
			compteur++;
		}
		else if (compteur == NBREGLE)
		{
			compteur = 0;
		}
	}
}

int main()
{
	initMenu();

	affichageResultat();

	return 0;
}